#include "owl/timer.h"

#if defined(__sun__) || defined(__rtems__)
  extern "C" int getrusage( int who, struct rusage *rusage );
#endif

OWLTimer::OWLTimer()
{
    userTime_ = systemTime_ = totalTime_ = 0.;
    state_ = Stopped;
}

void OWLTimer::reset()
{
    userTime_ = systemTime_ = totalTime_ = 0.;
    state_ = Stopped;
}

void OWLTimer::start()
{
    if ( state_ != Stopped )
    {
	return;
    }
    state_ = Running;
    gettimeofday( &start_, 0 );
    getrusage( RUSAGE_SELF, &start_usage_ );
}

void OWLTimer::stop()
{
    if ( state_ != Running )
    {
	return;
    }
    state_ = Stopped;
    getrusage( RUSAGE_SELF, &stop_usage_ );
    gettimeofday( &stop_, 0 );
    userTime_   += ( stop_usage_.ru_utime.tv_sec - start_usage_.ru_utime.tv_sec ) 
    		+ (stop_usage_.ru_utime.tv_usec - start_usage_.ru_utime.tv_usec)/1000000.;
    systemTime_ += ( stop_usage_.ru_stime.tv_sec - start_usage_.ru_stime.tv_sec ) 
    		+ (stop_usage_.ru_stime.tv_usec - start_usage_.ru_stime.tv_usec)/1000000.;
    totalTime_  += ( stop_.tv_sec-start_.tv_sec ) 
    		+ (stop_.tv_usec-start_.tv_usec)/1000000.;
}
