// 
// Added some error checking 
// Matthias 
// On plateforms that do not support the sem_init call but instead the sem_open one (Posix Real Time)
// Compile with the macro __USE_SEM_OPEN__
// The sem_p pointer is use for sanity checks 
// Storage is either static (sem_init) or done using the sem_open call
// if the initialisation somehow fails 
// or the object is destroyed, the pointer is set to NULL.
// 
// 

#include <owl/semaphore.h>
#include <stdio.h>
#include <assert.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <ers/ers.h>

ERS_DECLARE_ISSUE( owl, SystemCallFailed, "Call to the system function '" << name << "' has failed", ((std::string)name ) )

/* This function uses either sem_init or sem_open to initialise the semaphore
 * If initialisation was not sucessfull, sem_p is set to NULL 
 */

OWLSemaphore::OWLSemaphore(unsigned int initial)
{
    sem_p = 0; // by default, null pointer

#ifndef __USE_SEM_OPEN__
    const int err = sem_init( &sem_, 0, initial );
    if (err>=0) {
    	sem_p = &sem_;
	return; 
    } // sem_init sucessful
    ers::error( owl::SystemCallFailed( ERS_HERE, "sem_init" ) ); 
#else
    char file_name[64] ;
    snprintf( file_name, 63,"%x", (unsigned int) this );
    sem_p = sem_open( file_name, O_CREAT, S_IRWXU, initial ); 
    if ( sem_p != SEM_FAILED ) { return ; } 
    ers::error( owl::SystemCallFailed( ERS_HERE, "sem_open" ) ); 
#endif    
}
    
/* We use either sem_destroy or sem_close
 */
OWLSemaphore::~OWLSemaphore()
{
    if ( !sem_p )
    {
    	return;
    }
    
#ifndef __USE_SEM_OPEN__
    const int err = sem_destroy( sem_p );
#else 
    const int err = sem_close( sem_p );
#endif     
    sem_p = 0;
    if ( err >= 0 ) { 
    	return ;
    } 
#ifndef __USE_SEM_OPEN__  
    ers::error( owl::SystemCallFailed( ERS_HERE, "sem_destroy" ) ); 
#else
    ers::error( owl::SystemCallFailed( ERS_HERE, "sem_close" ) ); 
#endif
    
}
    
void OWLSemaphore::wait()
{
    ERS_ASSERT( sem_p );
    while ( sem_wait( sem_p ) < 0 && errno == EINTR )
    {	// On SLC3 a combination of SIGSTOP+SIGCONT interrupts sem_wait call
	// We don't want this and resume waiting.
	ERS_DEBUG( 1, "'sem_wait' was interrupted. Waiting will be resumed." ); 
    }
}

int OWLSemaphore::try_wait()
{
    ERS_ASSERT( sem_p );
    return sem_trywait( sem_p );
}

int OWLSemaphore::post()
{
    ERS_ASSERT( sem_p );
    return sem_post( sem_p );
}

int OWLSemaphore::get_value()
{
    ERS_ASSERT( sem_p );
    int val;
    sem_getvalue( sem_p, &val );
    return val;
}
