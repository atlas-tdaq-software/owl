#include <owl/time.h>

#include <stdio.h>
#include <sys/time.h>
#include <stdlib.h>

const struct tm
OWLDate::c_tm() const
{
  struct tm tt;
  localtime_r(&p_time, &tt);
  return tt;
}


int32_t
OWLDate::now()
{
  timeval tv;
  gettimeofday( &tv, 0 );
  set(&tv.tv_sec);
  return static_cast<int32_t>(tv.tv_usec);
}

////////////////////////////////////////////////////
// The method converts string 's' to time
// It is expected that the 's' is in the
// following format: dd/mm/[yy]yy hh:mm[:ss]
////////////////////////////////////////////////////

int32_t
OWLDate::set(const char * s)
{
  struct tm p_tm;

  p_tm.tm_isdst = -1;

  if(s == 0) {
    now();
    return 0;
  }

  p_tm.tm_hour = p_tm.tm_min = p_tm.tm_sec = 0;

    // skip leading spaces

  while(*s == ' ') ++s;


  const char * s2 = s;	// original time used in error reports
  char * nds = 0;	// non-digit symbol returned by strtol


    // find the day
  
  p_tm.tm_mday = strtol(s, &nds, 10);
  
  if(*nds != '/') {
    std::cerr << "ERROR [OWLDate::set()]:\n" << "failed to find the day in \'" << s2 << "\'\n";
    return 0;
  }
  else
    s = nds + 1;
  
  if(p_tm.tm_mday > 31 || p_tm.tm_mday == 0) {
    std::cerr << "ERROR [OWLDate::set()]:\n" << "bad day " << p_tm.tm_mday << " in \'" << s2 << "\'\n";
    return 0;
  }


    // find the month

  p_tm.tm_mon = strtol(s, &nds, 10);

  if(*nds != '/') {
    std::cerr << "ERROR [OWLDate::set()]:\n" << "failed to find the month in \'" << s2 << "\'\n";
    return 0;
  }
  else
    s = nds + 1;

  if(p_tm.tm_mon > 12 || p_tm.tm_mon == 0) {
    std::cerr << "ERROR [OWLDate::set()]:\n" << "bad month " << p_tm.tm_mon << " in \'" << s2 << "\'\n";
    return 0;
  }

  p_tm.tm_mon--;


    // find the year

  p_tm.tm_year = strtol(s, &nds, 10);

  if(p_tm.tm_year >= 0) {
    if(p_tm.tm_year < 70) p_tm.tm_year += 100;
    else if(p_tm.tm_year > 1900) p_tm.tm_year -= 1900;
  }

    // check if there is no data anymore

  if(*nds == 0)
    {
      set(&p_tm);
      return 0;
    }

  s = nds;


    // skip spaces

  while(*s == ' ') ++s;
  if(*s == 0) return 0;


    // find the hour

  p_tm.tm_hour = strtol(s, &nds, 10);

  if(*nds != ':') {
    std::cerr << "ERROR [OWLDate::set()]:\n" << "failed to find the hour in \'" << s2 << "\'\n";
    return 0;
  }
  else
    s = nds + 1;

  if(p_tm.tm_hour > 23) {
    std::cerr << "ERROR [OWLDate::set()]:\n" << "bad hour " << p_tm.tm_hour << " in \'" << s2 << "\'\n";
    return 0;
  }


    // find the minute

  p_tm.tm_min = strtol(s, &nds, 10);

  if(p_tm.tm_min > 59) {
    std::cerr << "ERROR [OWLDate::set()]:\n" << "bad minute " << p_tm.tm_min << " in \'" << s2 << "\'\n";
    return 0;
  }

  if(*nds != ':')
    {
      set(&p_tm);
      return 0;
    }


    // find the second

  p_tm.tm_sec = strtol(nds + 1, &nds, 10);

  if(p_tm.tm_sec > 59) {
    std::cerr << "ERROR [OWLDate::set()]:\n" << "bad second " << p_tm.tm_sec << " in \'" << s2 << "\'\n";
    return 0;
  }

  if(*nds != '.')
    {
      set(&p_tm);
      return 0;
    }

    // find the microsecond

  int32_t microseconds = static_cast<int32_t>(strtol(nds + 1, &nds, 10));

  if(microseconds > 1000000) {
    std::cerr << "ERROR [OWLDate::set()]:\n" << "bad microsecond " << microseconds << " in \'" << s2 << "\'\n";
    return 0;
  }

  set(&p_tm);

  return microseconds;
}


std::string
OWLDate::str() const
{
  char * cstr = c_str();
  std::string s( cstr );
  delete[] cstr;
  return s;
}

char *
OWLDate::c_str(const struct tm * t) const
{
  char *s = new char[32];
  struct tm p_tm;
  if(t == nullptr)
    {
      p_tm = c_tm();
      t = &p_tm;
    }

  if(s != 0) {
    sprintf( s, "%d/%d/%.2d", t->tm_mday, t->tm_mon + 1,
    		((t->tm_year < 100) ? t->tm_year : (t->tm_year - 100)));
  }
  return s;
}


////////////////////////////////////////////////////////////////////////////////
//	OWLTime
////////////////////////////////////////////////////////////////////////////////

char *
OWLTime::c_str(const struct tm * t) const
{
  struct tm p_tm;
  if(t == nullptr)
    {
      p_tm = c_tm();
      t = &p_tm;
    }

  char *s = OWLDate::c_str(t);

  if(s != 0) {
    char *s2 = s;
    while(*s2) s2++;

    if ( microseconds_ != 0 )
    	sprintf( s2, " %.2d:%.2d:%.2d.%.6d", t->tm_hour, t->tm_min, t->tm_sec, microseconds_ );
    else
    	sprintf( s2, " %.2d:%.2d:%.2d", t->tm_hour, t->tm_min, t->tm_sec );
  }

  return s;
}


std::ostream& operator<<(std::ostream& s, const OWLDate& d)
{
  char *str = d.c_str();

  s << str;
  delete [] str;
  
  return s;
}


std::ostream& operator<<(std::ostream& s, const OWLTime& t)
{
  char *str = t.c_str();

  s << str;
  delete [] str;
  
  return s;
}
