#include <stdlib.h>

#include "owl/regexp.h"
#include "owl/recore.h"

OWLRegexp::OWLRegexp( )
  : expr_( 0 )
{ ; }

OWLRegexp::OWLRegexp( const char * str )
  : string_( str )
{
    expr_ = RegExpCompiler().regcomp( str );
}

OWLRegexp::OWLRegexp( const std::string & str )
  : string_( str )
{
    expr_ = RegExpCompiler().regcomp( string_.c_str() );
}

OWLRegexp::~OWLRegexp( )
{
    if ( expr_ )
	::free ( const_cast<regexp *>(expr_) );
}

const OWLRegexp & 
OWLRegexp::operator=( const char * str )
{
    if ( expr_ )
	::free ( const_cast<regexp *>(expr_) );
    expr_ = RegExpCompiler().regcomp( str );
    string_ = str;
    return *this;
}

const OWLRegexp & 
OWLRegexp::operator=( const std::string & str )
{
    if ( expr_ )
	::free ( const_cast<regexp *>(expr_) );
    expr_ = RegExpCompiler().regcomp( str.c_str() );
    string_ = str;
    return *this;
}

int 
OWLRegexp::match( const char * str ) const
{
    if ( expr_ == 0 )
	return -1;
    regmatches rm;
    return RegExpWorker().regexec( expr_, str, &rm );
}

int 
OWLRegexp::match( const std::string & str ) const
{
    return match( str.c_str() );
}

int 
OWLRegexp::exact_match( const char * str ) const
{
    if ( expr_ == 0 )
	return -1;
    regmatches rm;
    int s = RegExpWorker().regexec( expr_, str, &rm );
    return ( (	s > 0 
    	     && rm.startp[0] == str 
	     && rm.endp[0] != 0 
	     && *(rm.endp[0]) == 0 )
	    	? 1 
		: 0 );
}

int 
OWLRegexp::exact_match( const std::string & str ) const
{
    return exact_match( str.c_str() );
}

std::string 
OWLRegexp::substring( const char * str, unsigned short start ) const
{
    regmatches rm;
    if (    expr_ == 0
	 || RegExpWorker().regexec( expr_, ( str + start ), &rm ) == 0 )
	return std::string();
    else
	return std::string ( rm.startp[0], (std::string::size_type)(rm.endp[0] - rm.startp[0]) );
}

std::string 
OWLRegexp::substring( const std::string & str, unsigned short start ) const
{
    return substring( str.c_str(), start );
}

OWLRegexp::Status 
OWLRegexp::status( ) const
{
    return ( expr_ == 0 ? OWLRegexp::Illegal : OWLRegexp::Ok );
}

const std::string &
OWLRegexp::str() const 
{ 
    return string_;
}
