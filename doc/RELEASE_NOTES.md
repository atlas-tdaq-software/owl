# owl

## tdaq-10-00-00

* Remove deprecated OWLMutex and OWLMutexRW classes. Use std::mutex and std::shared_mutex instead.
* Remove deprecated OWLThread. Use std::thread instead.
* Remove deprecated OWLCondition. Use std::condition_var instead.
