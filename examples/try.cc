#include <iostream>
#include <unistd.h>
#include <owl/regexp.h>
#include <owl/time.h>
#include <owl/timer.h>

#include <fnmatch.h>

int main( int , const char ** )
{
    long i;
    OWLTimer timer;
    timer.start();
    
    OWLRegexp r("Test.*Reg.* Expression");

    std::cout << "Testing Regular Expressions ..." << std::endl;
    if ( r.match( "Test String For Regular Expression" ) == 1 )
    {
	std::cout << "OK" << std::endl;
    }
    else
    {
	std::cout << "ERROR: match does not work correctly" << std::endl;
	return 1;
    }
    
    if ( r.exact_match( "Test String For Regular Expression" ) == 1 )
    {
	std::cout << "OK" << std::endl;
    }
    else
    {
	std::cout << "ERROR: exact_match does not work correctly" << std::endl;
	return 1;
    }
    
    if ( r.exact_match( "Test String For Regular Expression 123" ) == 1 )
    {
	std::cout << "ERROR: exact_match does not work correctly" << std::endl;
	return 1;
    }
    else
    {
	std::cout << "OK" << std::endl;
    }
    
    std::string sub = r.substring( "Test String For Regular Expression Pattern Matching" );
    std::cout << "Substring is: " << sub << std::endl;

    std::cout << "Testing Date ..." << std::endl;
    std::cout << " - current date is : " << OWLDate() << std::endl;

    OWLTime owl_time;

    std::cout << "Testing Time ..." << std::endl;
    std::cout << " - current time is : " << owl_time << std::endl;
    std::cout << " - number of seconds since \'00:00:00 UTC, January 1, 1970\' : "
		  << owl_time.c_time() << std::endl;

    OWLTime owl_time2(1000000001);
    std::cout << " - 1,000,000,001 seconds after \'00:00:00 UTC, January 1, 1970\' is : "
		  << owl_time2.c_str() << " " << owl_time2.c_time() 
                  << " " << owl_time2.total_mksec_utc() << std::endl;

    OWLTime owl_time22(1000000001, 1);
    std::cout << " - 1,000,000,001.000001 seconds after \'00:00:00 UTC, January 1, 1970\' is : "
		  << owl_time22.c_str() << " " << owl_time22.c_time() 
                  << " " << owl_time22.total_mksec_utc() << std::endl;

    OWLTime owl_time2a(owl_time2.c_str());
    std::cout << " - check constructor from string with seconds precision: \"" << owl_time2a << "\" => " << (owl_time2 == owl_time2a ? "PASSED" : "FAILED") << std::endl;

    OWLTime owl_time22a(owl_time22.c_str());
    std::cout << " - check constructor from string with micro-seconds precision: \"" << owl_time22a << "\" => " << (owl_time22 == owl_time22a ? "PASSED" : "FAILED") << std::endl;

    OWLTime owl_time3( OWLTime::Seconds );
    std::string str3 = owl_time3.str();
    std::cout << "time is " << owl_time3 << " " << owl_time3.str() << std::endl;
    std::cout << "time from string " << str3 << " is " << OWLTime( str3.c_str() ) << std::endl;
    
    OWLTime owl_time4( OWLTime::Microseconds );
    std::string str4 = owl_time4.str();
    std::cout << "time is " << owl_time4 << " " << owl_time4.str() << std::endl;
    std::cout << "time from string " << str4 << " is " << OWLTime( str4.c_str() ) << std::endl;
            
    OWLDate date;
    std::cout << "date is " << date << " " << date.str() << std::endl;    
    
    OWLTime ot;
    std::cout << "OWLTime now: " << ot 
    	      << " " << ot.c_time() 
              << " " << OWLTime( ot.c_time() ) << std::endl
              << " year:" << ot.year() << ", month:" << ot.month() << ", day:" << ot.day() << ", hour:" << ot.hour() << ", minute:" << ot.min() << ", second:" << ot.sec() << std::endl;

    for ( i = 0; i < 1000000; i++ )
    {
	char * data = new char[1024];
	delete [] data;
    }

    timer.stop();
    std::cout << "Time information:" << std::endl;
    std::cout << "* User time is   " << timer.userTime() << " seconds" << std::endl;
    std::cout << "* System time is " << timer.systemTime() << " seconds" << std::endl;
    std::cout << "* Total time is  " << timer.totalTime() << " seconds" << std::endl;

    std::cout << "Starting Timer again ..." << std::endl;
    timer.start();
    for ( i = 0; i < 1000000; i++ )
    {
	char * data = new char[1024];
	delete [] data;
    }
    timer.stop();
    std::cout << "* User time is   " << timer.userTime() << " seconds" << std::endl;
    std::cout << "* System time is " << timer.systemTime() << " seconds" << std::endl;
    std::cout << "* Total time is  " << timer.totalTime() << " seconds" << std::endl;

    std::cout << "Timing tests for regular expressions ..." << std::endl;
    std::cout << "testing OWLRegexp::match ..." << std::endl;
    
    
    timer.reset();
    timer.start();
    for ( i = 0; i < 100000; i++ )
    {
	if ( r.match( "Test String For Regular Expression" ) != 1 )
	{
	    std::cout << "ERROR: match does not work correctly" << std::endl;
	    break;
	}
    }
    timer.stop();
    std::cout << "* User time is   " << timer.userTime() << " seconds" << std::endl;
    std::cout << "* System time is " << timer.systemTime() << " seconds" << std::endl;
    std::cout << "* Real time is  " << timer.totalTime() << " seconds" << std::endl;

    std::cout << "testing OWLRegexp::exact_match ..." << std::endl;
    timer.reset();
    timer.start();
    for ( i = 0; i < 100000; i++ )
    {
	if ( r.exact_match( "Test String For Regular Expression" ) != 1 )
	{
	    std::cout << "ERROR: match does not work correctly" << std::endl;
	    break;
	}
    }
    timer.stop();
    std::cout << "* User time is   " << timer.userTime() << " seconds" << std::endl;
    std::cout << "* System time is " << timer.systemTime() << " seconds" << std::endl;
    std::cout << "* Real time is  " << timer.totalTime() << " seconds" << std::endl;

    return 0;
}

