# Online Wide Library (OWL)

The `owl` package consisted for historical reasons of at least three
different functional areas, and has been split after release tdaq-09-02-01.

The process management related functionality is now in the 
new [pmgsync](https://gitlab.cern.ch/atlas-tdaq-software/pmgsync) package.
If you include any of these headers check carefully if you really need
them, as most of this is handled internally by the various run control
libraries.

The [threadpool](https://gitlab.cern.ch/atlas-tdaq-software/threadpool)
package has been separated as well. As it is not used in TDAQ
itself, it is no longer part of the release. The git repository contains
a functioning package that can be dropped into a release if needed.
There is also a thread pool implementation in the [ipc](https://gitlab.cern.ch/atlas-tdaq-software/ipc/-/blob/master/ipc/threadpool.h)
package.

The following classes in the `owl` package have been deprecated and
will be removed in a future release:

  * OWLThread (use `std::thread` instead)
  * OWLMutex and related mutex and lock classes (use `std::mutex` etc. instead)
  * OWLCondition (use `std::condition_var` instead)

There is currently no replacement for `OWLSemaphore` in the C++17 
standard, however, there will be [one](https://en.cppreference.com/w/cpp/thread/counting_semaphore) 
in the upcoming C++20. If you only use the class to stop your main thread when a 
signal is received in another thread, consider using the new helper functions in
[ipc/signal.h](https://gitlab.cern.ch/atlas-tdaq-software/ipc/-/blob/master/ipc/signal.h) instead.
