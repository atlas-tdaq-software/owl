dnl $Id$ -*- m4 -*-
dnl $Name$
dnl
dnl COPYRIGHT (C) CERN, 1997.
dnl Author: Lassi A. Tuura
dnl
dnl ######################################################################
dnl This is a model for a hierarchical package configuration script.  To
dnl produce a `configure' script, run `autoconf' on it.  While you do so,
dnl make sure that the `aclocal.m4' is found from the SRT directory.  It
dnl contains the definitions of some of the macros used in this script.
dnl
dnl ######################################################################
dnl Before this script is run, it is expected that the user has properly
dnl set up his/her environment.  In other words `srt setup` should have
dnl been executed.
dnl
dnl ######################################################################
dnl Introduce revision number to the generated script.
AC_PREREQ(2.10)
AC_REVISION([$Id$])

dnl ######################################################################
dnl Initialisation
dnl
AC_INIT(PACKAGE)
SRT_INIT

dnl ######################################################################
dnl Configure locally for SRT compliance, and then configure the nested
dnl packages.  This includes any consistency checks that are necessary.
dnl `SRT_CONFIG_PACKAGES' is necessary only if there are subpackages in
dnl this package. `SRT_CONFIG_LOCAL' is sufficient for leaf packages
dnl that have uses.  Neither is necessary for leaf packages that do not
dnl use any other package (e.g. third party products).
dnl

SRT_CONFIG_LOCAL

dnl 
dnl SRT_CONFIG_PACKAGES
dnl

sh_lib_var=LD_LIBRARY_PATH
 
dnl ######################################################################
 
test_sh_lib_path="."
 
dnl ######################################################################

AC_SUBST(test_sh_lib_path)
AC_SUBST(sh_lib_var)

dnl ######################################################################
dnl Generate GNUmakefile, make state files, etc.  If
dnl `SRT_CONFIG_PACKAGES' found any nested packages, they will be
dnl recursed to.  If there are any configurable subdirectories, insert
dnl an appropriate `AC_CONFIG_SUBDIRS' statement just before
dnl `SRT_OUTPUT'.
dnl
SRT_OUTPUT(			\
	GNUmakefile		\
)
