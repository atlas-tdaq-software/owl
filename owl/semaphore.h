#ifndef OWL_SEMAPHORE_H
#define OWL_SEMAPHORE_H

#include <semaphore.h>

/** This class encapsulates a semaphore
 */

class OWLSemaphore
{
    sem_t	sem_;	        // we use static storage 
    sem_t	*sem_p  ;    // reference is null if there was a problem 
    
    // dummy copy constructor and operator= to prevent copying
    OWLSemaphore(const OWLSemaphore&);
    OWLSemaphore& operator=(const OWLSemaphore&);
    
  public:
 /** Initialises a semaphore
   * @param initial, initial value
   */
    OWLSemaphore(unsigned int initial = 0);
    
    /** Destroys the semaphore */
    ~OWLSemaphore();

/** suspends the calling thread until the semaphore has non-zero count. 
  * It then atomically decreases the semaphore count.
  */
    void wait();

 /** Non blocking variant of sem_wait
  * @return 0 if sucessful -1 otherwise see errno for why
  */
    int try_wait();

    /** atomically increases the count of the semaphore. */
    int post();

    /** @return the current count of the semaphore */
    int get_value();
};

#endif
