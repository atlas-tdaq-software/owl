#ifndef OWL_REGEXP_H
#define OWL_REGEXP_H

#include <string>

struct regexp;

class OWLRegexp 
{
    //
    // Dissallow object copying
    //
    OWLRegexp ( const OWLRegexp & );
    OWLRegexp & operator=( const OWLRegexp & );

  public:
    enum Status { Ok, Illegal };

    OWLRegexp( );
    OWLRegexp( const char * );
    OWLRegexp( const std::string & );
    ~OWLRegexp();

    const OWLRegexp & operator=( const char * );					// Recompiles pattern
    const OWLRegexp & operator=( const std::string & );					// Recompiles pattern
    
    int		    match( const char * str ) const;					// Pattern matching
    int		    match( const std::string & str ) const;				// Pattern matching
    int		    exact_match( const char * str ) const;				// Pattern matching
    int		    exact_match( const std::string & str ) const;			// Pattern matching
    std::string	    substring( const char * str, unsigned short start=0 ) const;	// Pattern matching
    std::string	    substring( const std::string & str, unsigned short start=0 ) const; // Pattern matching
    Status	    status() const;							// Return status

    const std::string & str() const;
        
  private:
    const regexp *	expr_;
    std::string		string_;
};

#endif
