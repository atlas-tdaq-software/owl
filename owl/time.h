#ifndef OWL_TIME_H
#define OWL_TIME_H

#include <time.h>
#include <stdint.h>
#include <iostream>

class OWLDate
{
  public:

     /** Construct date from struct tm **/
    OWLDate (const struct tm & t) : p_time(mktime(&const_cast<struct tm &>(t))) { ; }

    /** Construct date from time_t (number of seconds since Epoch) **/
    OWLDate (time_t clk) : p_time(clk) { ; }

    /** Construct date from string in format dd/mm/[yy]yy **/
    OWLDate (const char * s) {set(s);}

    /** Construct date from now **/
    OWLDate () {now();}

    bool operator==(const OWLDate &d) const { return (p_time == d.p_time) ; }
    bool operator!=(const OWLDate &d) const { return (p_time != d.p_time);  }
    bool operator<(const OWLDate &d) const { return (p_time < d.p_time); }
    bool operator<=(const OWLDate &d) const { return (p_time <= d.p_time); }
    bool operator>(const OWLDate &d) const { return (p_time > d.p_time); }
    bool operator>=(const OWLDate &d) const { return (p_time >= d.p_time); }

    friend std::ostream& operator<<(std::ostream&, const OWLDate&);

    /** Return year [1970-...]**/
    int year() const { return c_tm().tm_year + 1900; }

    /** Return month [1-12] **/
    int month() const {return c_tm().tm_mon + 1;}

    /** Return day [1-31] **/
    int day() const {return c_tm().tm_mday;}

    /** Return date as string **/
    virtual std::string str() const;

    /**
     * Return date as C-string.
     * \param t can be passed for efficiency.
     * Programmer is responsible to call delete [] on returned pointer.
     **/
    virtual char * c_str(const struct tm * t = nullptr) const;

    /** Return number of seconds since 00:00:00 UTC, January 1, 1970 **/
    time_t c_time() const { return p_time; }

    /** Return struct tm created from given date  **/
    const struct tm c_tm() const;

  protected:

    time_t p_time;

    int32_t now();
    int32_t set(const char *);
    void set(const struct tm * t) { if(t) {p_time = mktime(const_cast<struct tm *>(t));} else { now(); } }
    void set(const time_t * t) { if(t) { p_time = *t; } else { now(); } }
};


class OWLTime : public OWLDate
{
  public:

    enum Precision { Seconds, Microseconds };

    /** Construct time from struct tm, set microseconds to 0 **/
    OWLTime (const struct tm & t) : OWLDate(t), microseconds_(0) {;}

    /** Construct time from time_t (number of seconds since Epoch) and number of microseconds **/
    OWLTime (time_t seconds, time_t microseconds = 0) : OWLDate(seconds), microseconds_(microseconds) {;}

    /** Construct time from string in format dd/mm/[yy]yy hh:mm[:ss][.ms] **/
    OWLTime (const char * s) : microseconds_( set(s) ){;}

    /** Construct time from now **/
    OWLTime (Precision prc = Seconds) : microseconds_( now() ){ if (prc==Seconds) microseconds_=0;}

    bool operator==(const OWLTime &t) const { return (p_time == t.p_time && microseconds_ == t.microseconds_); }
    bool operator!=(const OWLTime &t) const {return !(*this == t);}
    bool operator<(const OWLTime &t) const { if (p_time != t.p_time) return (p_time < t.p_time); return (microseconds_ < t.microseconds_); }
    bool operator<=(const OWLTime &t) const {return (*this == t || *this < t);}
    bool operator>(const OWLTime &t) const {return !(*this < t);}
    bool operator>=(const OWLTime &t) const {return (*this == t || *this > t);}

    friend std::ostream& operator<<(std::ostream&, const OWLTime&);

    /** Return the number of hours past midnight in the range 0 to 23 **/
    int hour() const { return c_tm().tm_hour; }

    /** Return the number of minutes after the hour, in the range 0 to 59 **/
    int min() const { return c_tm().tm_min; }

    /** Return the number of seconds after the minute, normally in the range 0 to 59, but can be up to 60 to allow for leap seconds **/
    int sec() const { return c_tm().tm_sec; }

    /** Return the number of microseconds after the second, in the range 0 to 999999 **/
    uint32_t mksec() const { return microseconds_; }

    /** Return the number of microseconds since Epoch **/
    uint64_t total_mksec_utc() const { return (static_cast<uint64_t>(p_time)*1000000 + static_cast<uint64_t>(microseconds_)); }

    /**
     * Return time as C-string.
     * \param t can be passed for efficiency.
     * Programmer is responsible to call delete [] on returned pointer.
     **/
    virtual char * c_str(const struct tm * t = nullptr) const;

  private:
    uint32_t    microseconds_;
};
    
#endif
