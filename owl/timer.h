#ifndef OWL_TIMER_H
#define OWL_TIMER_H
#include <sys/time.h>
#include <sys/resource.h>

class OWLTimer
{
  public:
    OWLTimer( );
    
    void	start( );
    void	reset( );
    void	stop( );
    double	userTime( ) const { return userTime_; }
    double	totalTime( ) const { return totalTime_; }
    double	systemTime( ) const { return systemTime_; }

  private:
    enum State { Stopped, Running };
    
    double		userTime_;    
    double		systemTime_;    
    double		totalTime_;    
    struct timeval	start_, stop_;    
    struct rusage	start_usage_, stop_usage_;
    State		state_;
};

#endif
