#ifndef OWL_RECORE_H
#define OWL_RECORE_H

#define NSUBEXP  10

struct regexp
{
    char regstart;		// Internal use only.
    char reganch;		// Internal use only.
    const char *regmust;	// Internal use only.
    int  regmlen;		// Internal use only.
    char program[1];		// Unwarranted chumminess with compiler.
};

struct regmatches
{
    const char *startp[NSUBEXP];
    const char *endp[NSUBEXP];
};

class RegExpCompiler
{
    const char *regparse;	/* Input-scan pointer. */
    int regnpar;		/* () count. */
    char *regcode;		/* Code-emit pointer; &regdummy = don't. */
    long regsize;		/* Code size. */
  
    char* reg( int paren, int* flagp);
    char* regbranch( int* flagp);
    char* regpiece( int* flagp);
    char* regatom( int* flagp);
    char* regnode( char op);
    void regc( unsigned char c);
    void reginsert( char op, char* opnd);
    void regtail( char* p, const char* val);
    void regoptail( char* p, const char* val);
  
  public:
    const regexp * regcomp ( const char * re );
};

class RegExpWorker
{
    const char *reginput;	/* String-input pointer. */
    const char *regbol;	/* Beginning of input, for ^ check. */
    const char **regstartp;	/* Pointer to startp array. */
    const char **regendp;	/* Ditto for endp. */

    int regtry( const regexp* prog, const char* string, regmatches* matches);
    int regmatch( const char* prog);
    int regrepeat( const char* p);
    
  public:
    int regexec( const regexp* prog, const char* string, regmatches* matches);
};

#endif

